/**
 * Created by btesila on 11/16/2014.
 */

$(document).ready(function() {
        //Better to construct options first and then pass it as a parameter
        console.log("Set options for graph");

        var request = $.ajax({
            url : "http://localhost:8080/bucharestjaguars/analytics/getAnalytics",
            type : "get",
            traditional : "true",
            data : {
                "goalName":"exercise"
            }
        });
        request.done(function(response) {
            console.log(response);
            var data = response.dataPoints;
            var dataPointsArray = [];
            for(var i in data){
                dataPointsArray.push({x: data[i].x, y: data[i].y})
            }
            console.log(dataPointsArray);

            var options = {
                title: {
                    text: "Exercise Progress"
                },
                data: [
                    {
                        type: "spline", //change it to line, area, column, pie, etc
                        dataPoints: [
                            { x: 10, y: 10 },
                            { x: 20, y: 12 },
                            { x: 30, y: 8 },
                            { x: 40, y: 14 },
                            { x: 50, y: 6 },
                            { x: 60, y: 24 },
                            { x: 70, y: -4 },
                            { x: 80, y: 10 },
                        ]
                    }
                ]
            };

            $("#chartContainer").CanvasJSChart(options);

        });


});

$('.recomm').click(function(){
    console.log("REC CLICKED");

    var goalName =  $(this).attr('id');
    console.log(goalName);

    $("#recomm-div").empty();


    var request = $.ajax({
        url : "http://localhost:8080/bucharestjaguars/recommendations/retrieve",
        type : "get",
        traditional : "true",
        data : {
            "goalName": goalName
        }
    });
    request.done(function(response) {
        console.log(response);
        for(var i in response.recommendations){
            console.log(response.recommendations[i]);
            $("#recomm-div").append( "<div class='alert alert-success'>" +
            response.recommendations[i] +
            "</div>" );
        }




    });


});