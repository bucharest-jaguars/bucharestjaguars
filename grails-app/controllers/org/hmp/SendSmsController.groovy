package org.hmp

import grails.plugins.springsecurity.Secured

class SendSmsController {

    def springSecurityService
    def smsService

    def index() {
    }


    def sendSms() {
        def userX =  springSecurityService.getCurrentUser()
        def statusX=Status.findByUser(userX)
        def goalX=Goal.findByName("exercise")

        def c = History.createCriteria()
        def results = c.list {
                eq("user", userX)
                eq("goal", goalX)
            order("date", "desc")
        }

        def currentValue = results?.get(0)?.value

        def prevValue = results?.get(1)?.value


        def messageHeader = "Dear "+userX.firstName+", "
        def messageBody

        if (currentValue == statusX?.toAchieve){
            messageBody = "Congrats! You've reached your goal. Keep exercising !"
        }else if (currentValue < statusX?.toAchieve){
            if ((currentValue - prevValue) < 0){
                messageBody = "Your exercising level decreased. Work harder to achieve your goal!"
            }else{
                messageBody = "Continue increasing your exercising level. You are on the right path"
            }
        }

        def message = messageHeader + messageBody
        smsService.send("+40741273393",message)

        redirect uri: '/recommendations/recommendations'
    }
}
