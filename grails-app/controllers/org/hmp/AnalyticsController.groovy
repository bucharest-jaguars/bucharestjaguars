package org.hmp

class AnalyticsController {


    def springSecurityService


    def analytics() {
        render(view: "analytics", model: [active: "analytics", userInstance: springSecurityService.getCurrentUser()])
    }

        def getAnalytics() {
            render(contentType: "text/json") {
                def goalX
                goalX =  Goal.findByName(params?.goalName)
                dataPoints = array {
                    for (h in History.findAllByGoal(goalX)) {
                        history "x": h.date.day, "y": h.value
                    }
                }
            }
        }
    }

