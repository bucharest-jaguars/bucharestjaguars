package org.hmp

import grails.converters.JSON
import org.codehaus.groovy.grails.web.json.JSONObject

class RecommendationsController {

    def springSecurityService

    def recommendations() {
        render(view: "recommendations", model: [active: "recommendations", userInstance: springSecurityService.getCurrentUser()])
    }

    def retrieve(){
        def goalX
        if (params.goalName) {
            goalX =  Goal.findByName(params?.goalName)
        } else {
            goalX = Goal.findByName("exercise")
        }
        def c = Content.createCriteria()
        def results = c.list {
            eq("goal", goalX)
        }
        def resultsData = new ArrayList()
        for(var in results){
            resultsData.add(var.data)
        }
        render([recommendations: resultsData] as JSON)
    }

}
