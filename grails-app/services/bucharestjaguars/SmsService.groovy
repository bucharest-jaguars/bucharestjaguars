package bucharestjaguars
import groovyx.net.http.*
import org.codehaus.groovy.grails.commons.GrailsApplication

class SmsService {

    def grailsApplication
    def roomrentHttpEndpointBean = new HTTPBuilder("https://api.twilio.com/2010-04-01/")

    static transactional = true

    def send(String destinationPhoneNumber, String message) {
        roomrentHttpEndpointBean.auth.basic(
                grailsApplication.config.twilio?.account.sid,
                grailsApplication.config.twilio?.account.auth_token)

        def result = roomrentHttpEndpointBean.request(Method.POST) { req ->
            requestContentType = ContentType.URLENC
            uri.path = "Accounts/${grailsApplication.config.twilio?.account.sid}/SMS/Messages.json"
            if(grailsApplication.config.twilio?.sms?.enable_status_callback) {
                body = [ To: destinationPhoneNumber,
                         From: grailsApplication.config.twilio?.phones.main,
                         Body: message,
                         StatusCallback: "${grailsApplication.config.grails.serverURL}/sms/callback" ]
            } else {
                body = [ To: destinationPhoneNumber, From: grailsApplication.config.twilio?.phones.main, Body: message ]
            }
            response.success = { resp, data ->
                return [status: data.status, sid: data.sid]
            }
            response.failure = { resp, data ->
                return [status: data.status, code: data.message]
            }
        }
    }
}