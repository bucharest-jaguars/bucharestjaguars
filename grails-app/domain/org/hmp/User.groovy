package org.hmp

class User {

	transient springSecurityService

    static hasMany = [goal: Goal]

	String firstName
    String lastName
    String sex
    String username
	String password
	boolean enabled
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static constraints = {
        firstName(nullable: true, blank: true)
        lastName(nullable: true, blank: true)
        sex(nullable: true, blank: true,inList: ["M", "F"])
		username blank: false, unique: true
		password blank: false
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Role> getAuthorities() {
		UserRole.findAllByUser(this).collect { it.role } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}

    public String toString() {
        /*"${username}"+": "+*/"${firstName}"+" "+"${lastName}"
    }
}
