package org.hmp

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class History {

    User user
    Date date
    Goal goal
    int value

    static constraints = {
        user()
        date()
        goal()
        value()
    }

    public String toString() {
        "${user}"+" "+"${date}"+" "+"${goal}"+" "+"${value}"
    }
}
