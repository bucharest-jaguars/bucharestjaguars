package org.hmp

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils

class Content {

    Goal goal
    Level level
    URI uri
    String data
    static mapping = {
        data type: "text"
    }

    static constraints = {
        goal()
        level()
        uri(nullable: true, blank: true)
        data(nullable: true, blank: true)
    }

    public String toString() {
        "${goal}"+" "+"${level}"+" "+"${uri}"+" "+"${data}"
    }
}
