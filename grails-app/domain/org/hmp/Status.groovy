package org.hmp

class Status {

    User user
    Level level
    Goal goal
    int now
    int toAchieve

    static constraints = {
        user()
        level()
        goal()
        now()
        toAchieve()
    }

    public String toString() {
        "${goal}"+" "+"${level}"
    }
}
