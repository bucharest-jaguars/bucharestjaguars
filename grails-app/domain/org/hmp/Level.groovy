package org.hmp

class Level {

    String name

    static constraints = {
        name(nullable: false, blank: false)
    }
}
