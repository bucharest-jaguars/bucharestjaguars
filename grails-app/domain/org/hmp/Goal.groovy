package org.hmp

class Goal {

    String name
    int threshold1
    int threshold2

    static constraints = {
        name(nullable: false, blank: false)
        threshold1(nullable: false, blank: false)
        threshold2(nullable: false, blank: false)
    }

    public String toString() {
        "${name}"
    }
}
