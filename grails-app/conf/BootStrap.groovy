import org.hmp.Content
import org.hmp.Goal
import org.hmp.History
import org.hmp.Level
import org.hmp.Role
import org.hmp.Status
import org.hmp.User
import org.hmp.UserRole

import java.text.DateFormat
import java.text.SimpleDateFormat

class BootStrap {

    def springSecurityService

    def init = { servletContext ->

        def userRole = Role.findByAuthority('ROLE_USER') ?: new Role(authority: 'ROLE_USER').save(failOnError: true)
        def adminRole = Role.findByAuthority('ROLE_ADMIN') ?: new Role(authority: 'ROLE_ADMIN').save(failOnError: true)

        def normalUser = User.findByUsername('Batman') ?: new User(
                username: 'Batman',
                password: "pass",
                firstName: "Bruce",
                lastName: "Wayne",
                sex: "M",
                enabled: true).save(failOnError: true)
        def normalUser2 = User.findByUsername('Catwoman') ?: new User(
                username: 'Catwoman',
                password: "pass",
                firstName: "Selina",
                lastName: "Kyle",
                sex: "F",
                enabled: true).save(failOnError: true)
        def normalUser3 = User.findByUsername('Ironman') ?: new User(
                username: 'Ironman',
                password: "pass",
                firstName: "Tony",
                lastName: "Stark",
                sex: "M",
                enabled: true).save(failOnError: true)
        def normalUser4 = User.findByUsername('Superman') ?: new User(
                username: 'Superman',
                password: "pass",
                firstName: "Clark",
                lastName: "Kent",
                sex: "M",
                enabled: true).save(failOnError: true)
        def adminUser = User.findByUsername('admin') ?: new User(
                username: 'admin',
                password: "pass",
                firstName: "Admin",
                lastName: "Default",
                sex: "M",
                enabled: true).save(failOnError: true)

        //user
        if (!normalUser.authorities.contains(userRole)) {
            UserRole.create normalUser, userRole, true
        }
        //admin
        if (!adminUser.authorities.contains(userRole)) {
            UserRole.create adminUser, userRole, true
        }
        if (!adminUser.authorities.contains(adminRole)) {
            UserRole.create adminUser, adminRole, true
        }

        def lowLevel = Level.findByName('Low') ?: new Level(name: 'Low').save(failOnError: true)
        def mediumLevel = Level.findByName('Medium') ?: new Level(name: 'Medium').save(failOnError: true)
        def highLevel = Level.findByName('High') ?: new Level(name: 'High').save(failOnError: true)

        def goalWeight = Goal.findByName('weight') ?: new Goal(name: 'weight', threshold1:5, threshold2:10).save(failOnError: true)
        def uri1 = new URI("")
        def data1 = "Do you need to lose a few kg in order to get into that tight jeans? Eating lots of protein, fiber, and healthy carbs can help you with that."
        def contentWeightLowLevel1 = new Content(goal: goalWeight, level: lowLevel, uri: uri1, data: data1).save(failOnError: true)
        def data2 = "Embrace the green side, my apprentice. Enjoy a healthy, plant-based diet with lots of fresh fruits and vegetables packed with fiber."
        def contentWeightLowLevel2 = new Content(goal: goalWeight, level: lowLevel, uri: uri1, data: data2).save(failOnError: true)
        def data3 = "Move it people! Along with modified eating, incorporate physical activity into your lifestyle. Move throughout the day. Stretch while you make breakfast. Take the stairs at the office. Go for a stroll during your lunch break. It's a myth that you need to sit to digest your food"
        def contentWeightLowLevel3 = new Content(goal: goalWeight, level: lowLevel, uri: uri1, data: data3).save(failOnError: true)
        def data4 = "Squeeze exercise into your busy life. Don't talk yourself out of exercise because of lack of time, Set realistic goals that you can build on—one squat is better than no squats. Brushing your teeth twice a day should take you four minutes. You can do about 60 squats in that time. Burning calories while brushing your teeth now? Now that’s weight loss everyone can achieve!"
        def contentWeightMediumLevel4 = new Content(goal: goalWeight, level: mediumLevel, uri: uri1, data: data4).save(failOnError: true)
        def data5 = "Bicycling is another low-impact, high-rewards activity for losing weight."
        def contentWeightMediumLevel5 = new Content(goal: goalWeight, level: mediumLevel, uri: uri1, data: data5).save(failOnError: true)
        def data6 = "Vigorous swimming can burn anywhere from 400 to 700 calories an hour. All types of swimming are effective for helping you shed pounds, from a front crawl to a breast stroke or even the dog paddle. Swimming is a highly effective exercise for weight loss and toning. It’s one of the lowest-impact exercises out there, and it strengthens, tones and conditions your whole body."
        def contentWeightMediumLevel6 = new Content(goal: goalWeight, level: mediumLevel, uri: uri1, data: data6).save(failOnError: true)
        def data7 = "Formulate a workout plan. Aim for three 40-minute workouts each week, at specific times. Your exercise schedule needs to be as important as any other appointment you have. Vary your routine — maybe it's tennis one day and a dance class on another "
        def contentWeightHighLevel7 = new Content(goal: goalWeight, level: highLevel, uri: uri1, data: data7).save(failOnError: true)
        def data8 = "Do a reality check. Be completely honest with yourself about how fit you are. Getting in shape is a personal journey that requires a realistic self-perspective; some novice exercisers are overly ambitious and begin a workout plan or set goals that are too hard on their bodies. So take it slow. Keep your focus on where you are and the progress you've made."
        def contentWeightHighLevel8 = new Content(goal: goalWeight, level: highLevel, uri: uri1, data: data8).save(failOnError: true)
        def data9 = "Eat right. Make sure you're eating the best foods to fuel your body — including lots of fresh fruits and veggies, lean proteins, and healthy fats. And drink at least six cups of water a day — or more on the days you work out — to keep your hardworking body hydrated and energized."
        def contentWeightHighLevel9 = new Content(goal: goalWeight, level: highLevel, uri: uri1, data: data9).save(failOnError: true)

        def goalStress = Goal.findByName('stress') ?: new Goal(name: 'stress', threshold1:3, threshold2:7).save(failOnError: true)
        def uri2 = new URI("http://www.calm.com")
        def uri3 = new URI("http://thequietplaceproject.com/thequietplace")
        def data10 = "Take a deep breath. If you’re feeling overwhelmed or you are coming out of a tense meeting and need to clear your head, a few minutes of deep breathing will restore balance. Simply inhale for five seconds, hold and exhale in equal counts through the nose. It’s like getting the calm and focus of a 90-minute yoga class in three minutes or less at your desk."
        def contentStressLowLevel10 = new Content(goal: goalStress, level: lowLevel, uri: uri1, data: data10).save(failOnError: true)
        def data11 = "Change your story. Your perspective of stressful office events is typically a subjective interpretation of the facts, often seen through the filter of your own self-doubt. However, if you can step back and take a more objective view, you’ll be more effective and less likely to take things personally."
        def contentStressLowLevel11 = new Content(goal: goalStress, level: lowLevel, uri: uri1, data: data11).save(failOnError: true)
        def data12 = "Prioritize your priorities. With competing deadlines and fast-changing priorities, it’s critical to define what’s truly important and why. That requires clarity. It’s important to understand your role in the organization, the company’s strategic priorities, and your personal goals and strengths. Cull your to-do list by focusing on those projects that will have the most impact and are best aligned with your goals."
        def contentStressLowLevel12 = new Content(goal: goalStress, level: lowLevel, uri: uri1, data: data12).save(failOnError: true)
        def data13 = "Create an Oasis. An absurdly easy way to get reduce that stress is to shut down your computer and your cell–not just while you sleep, but also an hour before and after you sleep. This takes discipline, because you're probably in habit of checking email, texts and so forth. This also takes self-confidence, because you must believe that you need to be at the constant beck and call of your boss, colleagues and customers. Do it anyway."
        def contentStressMediumLevel13 = new Content(goal: goalStress, level: mediumLevel, uri: uri1, data: data13).save(failOnError: true)
        def data14 = "Renegotiate your Workload. Unreasonable expectations of what you're capable of accomplishing are a huge source of stress–regardless of whether those expectations come from yourself, from your boss, or from your customers. The cure for this kind of stress is a dose of reality. Look at how much time you've got to spend, assess the amount of work that needs to be done, and, based on that, be realistic about what's actually going to get done. If you're expected to accomplish A,B,C and D, and there's only time to achieve three of the four, decide–or force your boss to decide–which three will actually get done and which one will not."
        def contentStressMediumLevel14 = new Content(goal: goalStress, level: mediumLevel, uri: uri1, data: data14).save(failOnError: true)
        def data15 = "Disconnect from the Uncontrollable. There are always events that you simply can't control: the economy, traffic, politics, other people's emotions, customer decisions, and so forth. While it can be useful to observe and predict such events (in order to know how to react to them), once you've decided how you'll deal with them, it's stressful (and, frankly, a little nutso) to continue to focus on them. Worrying about stuff you can't control isn't going to make an iota of difference either in the short or the long run. It's wasted energy and extra stress you don't need. Change what can change and shrug off what you can't."
        def contentStressMediumLevel15 = new Content(goal: goalStress, level: mediumLevel, uri: uri1, data: data15).save(failOnError: true)
        def data16 = "The stress is killing you? Take a break and try a relaxing site for a couple of minutes: http://www.calm.com/ "
        def contentStressHighLevel16 = new Content(goal: goalStress, level: highLevel, uri: uri2, data: data16).save(failOnError: true)
        def data17 = "Try to find a couple of minutes each 2-3 and engage yourself in a friendly game on your work place. For example Playing table tennis is an intense physical activity and it does not lack passion either, making it perfect for relaxing your mind after a stressful day at the office. An excellent means to blow off some steam, ping pong can relieve stress and ameliorate the symptoms of depression as well. The act of hitting the ball with the paddle in itself is able to stimulate the brain to release endorphin and to promote feelings of satisfaction and happiness. Another feature of table tennis through which it is able to reduce stress levels is that it distracts the players completely from stress factors coming from the external world."
        def contentStressHighLevel7 = new Content(goal: goalStress, level: highLevel, uri: uri1, data: data17).save(failOnError: true)
        def data18 = "For relaxing a few minutes, take a trip into the quiet place: http://thequietplaceproject.com/thequietplace There are several pages on offer, including the 90 Seconds Relaxation Exercise, the Quiet Place, and the Thoughts Room, all of which offer variations on the theme of gently persuading you to take a moment to relax and rid yourself of any worries."
        def contentStressHighLevel8 = new Content(goal: goalStress, level: highLevel, uri: uri3, data: data18).save(failOnError: true)

        def goalExercise = Goal.findByName('exercise') ?: new Goal(name: 'exercise', threshold1:2, threshold2:4).save(failOnError: true)
        def uri4 = new URI("https://www.youtube.com/watch?v=nZy9qSVVQSM")
        def data19 = "Look, some fun facts. Read them in order!"
        //uri from drive with picture needs to be inserted
        def contentExerciseLowLevel19 = new Content(goal: goalExercise, level: lowLevel, uri: uri1, data: data19).save(failOnError: true)
        def data20 = "Try to relax at the office by doing this simple stretches"
        //uri from drive with picture needs to be inserted
        def contentExerciseLowLevel20 = new Content(goal: goalExercise, level: lowLevel, uri: uri1, data: data20).save(failOnError: true)
        def data21 = "It’s easy to find ways to exercise each day: take the stairs instead of the elevator, go eat outside instead of eating at the office, if you live close, just walk to the office. "
        def contentExerciseLowLevel21 = new Content(goal: goalExercise, level: lowLevel, uri: uri1, data: data21).save(failOnError: true)
        def data22 = "Standing burns more calories than sitting does. Look for ways to get out of your chair. Stand while talking on the phone. Skip instant messaging and email, and instead walk to a colleague's desk for a face-to-face chat."
        def contentExerciseMediumLeve22 = new Content(goal: goalExercise, level: mediumLevel, uri: uri1, data: data22).save(failOnError: true)
        def data23 = "Consider trading your desk chair for a firmly inflated fitness or stability ball, as long as you're able to safely balance on the ball. You'll improve your balance and tone your core muscles while sitting at your desk. Use the fitness ball for wall squats or other exercises during the day."
        def contentExerciseMediumLevel23 = new Content(goal: goalExercise, level: mediumLevel, uri: uri1, data: data23).save(failOnError: true)
        def data24 = "Store resistance bands — stretchy cords or tubes that offer weight-like resistance when you pull on them — or small hand weights in a desk drawer or cabinet. Do arm curls between meetings or tasks."
        def contentExerciseMediumLevel24 = new Content(goal: goalExercise, level: mediumLevel, uri: uri1, data: data24).save(failOnError: true)
        def data25 = "Yogis have been using and sharing time-tested strategies for thousands of years to improve communication. Now, business leaders are following their lead ... and seeing the results at work and in life. https://www.youtube.com/watch?v=nZy9qSVVQSM "
        def contentExerciseHighLevel25 = new Content(goal: goalExercise, level: highLevel, uri: uri4, data: data25).save(failOnError: true)


        def goalOutdoor = Goal.findByName('outdoor') ?: new Goal(name: 'outdoor', threshold1:3, threshold2:6).save(failOnError: true)
        def data26 = "Jogging is a form of trotting or running at a slow or leisurely pace. The main intention is to increase physical fitness with less stress on the body than from faster running, or to maintain a steady speed for longer periods of time. Performed over long distances, it is a form of aerobic endurance training."
        def contentOutdoorLowLevel26 = new Content(goal: goalOutdoor, level: lowLevel, uri: uri1, data: data26).save(failOnError: true)
        def data27 = "Cycling is one of the easiest ways to exercise. You can ride a bicycle almost anywhere, at any time of the year, and without spending a fortune. According to the British Medical Association, cycling just 20 miles a week can reduce the risk of coronary heart disease by 50%."
        def contentOutdoorLowLevel27 = new Content(goal: goalOutdoor, level: lowLevel, uri: uri1, data: data27).save(failOnError: true)
        def data28 = "Paintball is a strategy game where you run around with guns that shoot paint pellets. You know when you have been hit, as it leaves a paint mark, and also hurts! Still, it is fantastic fun! This sport combines strategy, teamwork and skill into an exciting action packed day. It is usually played outside in woodland areas or fields."
        def contentOutdoorMediumLevel28 = new Content(goal: goalOutdoor, level: mediumLevel, uri: uri1, data: data28).save(failOnError: true)
        def data29 = "Go kart racing is the most economic form of motorsport available. As a recreational activity, go karting can be done by almost anybody. The idea is to race around the go kart track as quickly as possible for a limited number of laps."
        def contentOutdoorMediumLevel29 = new Content(goal: goalOutdoor, level: mediumLevel, uri: uri1, data: data29).save(failOnError: true)
        def data30 = "Climbing is simply the activity of using your hands and feet to surmount a steep obstacle such as an artificial wall, boulder, cliff, or mountain. Usually done for recreational enjoyment, fun, and sport, climbing allows you to fully experience the great outdoors by giving you eagle-eye views from lofty summits, pushing both your physical fitness and mental health"
        def contentOutdoorHighLevel30 = new Content(goal: goalOutdoor, level: highLevel, uri: uri1, data: data30).save(failOnError: true)
        def data31 = "Fishing often involves travelling to beautiful quiet locations and sitting looking out upon the water, whilst waiting for a fish to bite. The location with its sights, sounds and smells can be instantly relaxing. Imagine closing your eyes and hearing just the slow rise and fall of the waves and inhaling the scent of the sea air. Fishing provides time to think and is an escape from a hectic day job or from having to deal with screaming kids."
        def contentOutdoorHighLevel31 = new Content(goal: goalOutdoor, level: highLevel, uri: uri1, data: data31).save(failOnError: true)


        //-----------------------------------
        String inputStr = "10-11-2014";
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date date1 = dateFormat.parse(inputStr);
        Date date2 = dateFormat.parse("11-11-2014");
        Date date3 = dateFormat.parse("12-11-2014");
        Date date4 = dateFormat.parse("13-11-2014");
        Date date5 = dateFormat.parse("15-11-2014");
        Date date6 = dateFormat.parse("18-11-2014");
        Date date7 = dateFormat.parse("19-11-2014");
        def h1 = new History(user : normalUser, date: date1, goal: goalExercise, value: 4).save(failOnError: true)
        def h2 = new History(user : normalUser, date: date2, goal: goalExercise, value: 3).save(failOnError: true)
        def h3 = new History(user : normalUser, date: date3, goal: goalExercise, value: 1).save(failOnError: true)
        def h4 = new History(user : normalUser, date: date4, goal: goalExercise, value: 2).save(failOnError: true)
        def h5 = new History(user : normalUser, date: date5, goal: goalExercise, value: 4).save(failOnError: true)
        def h6 = new History(user : normalUser, date: date6, goal: goalExercise, value: 3).save(failOnError: true)
        def h7 = new History(user : normalUser, date: date7, goal: goalExercise, value: 1).save(failOnError: true)
        def h8 = new History(user : normalUser, date: new Date(), goal: goalExercise, value: 2).save(failOnError: true)

        //New status
        def status1 = new Status(user: normalUser, goal: goalExercise, level: lowLevel, now: 1, toAchieve: 4).save(failOnError: true)

    }
    def destroy = {
    }
}
