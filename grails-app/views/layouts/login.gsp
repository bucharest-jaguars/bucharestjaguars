<%--
  Created by IntelliJ IDEA.
  User: btesila
  Date: 11/16/2014
  Time: 4:28 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:layoutTitle default="Grails"/></title>
    <!-- Bootstrap Core CSS -->

    <link href="${resource(dir: 'css', file: 'bootstrap.min.css')}" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="${resource(dir: 'css', file: 'styles.css')}" rel="stylesheet">

    <g:layoutHead/>
</head>

<body style = "background: url('https://lh4.googleusercontent.com/-tUvzkbsqMsU/VGhYT9wJv1I/AAAAAAAAEP4/67SB5alWGlI/w1532-h865-no/loginPage.jpg')">

<g:layoutBody/>

<!-- Bootstrap Core JavaScript -->
<script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>

<script src="${resource(dir: 'js', file: 'jquery.min.js')}"></script>

</body>
</html>