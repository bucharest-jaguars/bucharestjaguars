<%--
  Created by IntelliJ IDEA.
  User: btesila
  Date: 11/15/2014
  Time: 9:46 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title><g:layoutTitle default="Grails"/></title>
    <!-- Bootstrap Core CSS -->

    <link href="${resource(dir: 'css', file: 'bootstrap.min.css')}" rel="stylesheet">

    <!-- Custom CSS -->

    <link href="${resource(dir: 'css', file: 'sb-admin.css')}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="${resource(dir: 'css', file: 'morris.css')}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="${resource(dir: 'css', file: 'font-awesome.min.css')}" rel="stylesheet" type="text/css">

    <!-- OWL Tabs -->
    <link href="${resource(dir: 'css', file: 'owl.carousel.css')}" rel="stylesheet" type="text/css">
    <link href="${resource(dir: 'css', file: 'owl.theme.css')}" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <g:layoutHead/>
</head>

<body>
<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" style="color:#3fbf79" href="index.html">Office Pulse</a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">

    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
        <ul class="dropdown-menu alert-dropdown">
            <li>
                <a href="${createLink(uri:'/sendSms/sendSms')}" style="width: 20px"><span class="label label-default" >Send SMS</span></a>
            </li>
        </ul>
    </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ${userInstance?.firstName} &nbsp ${userInstance?.lastName}  <b
                        class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="${createLink(uri:'/user/show/'+ userInstance?.id)}"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="${createLink(uri:'/user/list/')}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="${active=='recommendations' ? 'active' : ''}">
                    %{--<g:link controller="hello'/'index"><i class="fa fa-star-o"></i> Recommendations</g:link>--}%
                    <a href="${createLink(uri:'/recommendations/recommendations')}"><i class="fa fa-star-o"></i> Recommendations</a>
                </li>
                <li class="${active=='analytics' ? 'active' : ''}">
                    <a href="${createLink(uri:'/analytics/analytics')}"><i class="fa fa-fw fa-bar-chart-o"></i> Analytics</a>
                </li>

            </ul>
            %{--<g:layoutBody/>--}%
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">

        <div class="container-fluid">

            <g:layoutBody/>

        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="${resource(dir: 'js', file: 'jquery.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.carousel.js')}"></script>
<script src="${resource(dir: 'js', file: 'owl.js')}"></script>
<script src="${resource(dir: 'js', file: 'jquery.canvasjs.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'graph.js')}"></script>


<!-- Bootstrap Core JavaScript -->
<script src="${resource(dir: 'js', file: 'bootstrap.min.js')}"></script>

<!-- Morris Charts JavaScript -->
<script src="${resource(dir: 'js', file: 'raphael.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'morris.min.js')}"></script>
<script src="${resource(dir: 'js', file: 'morris-data.js')}"></script>

</body>
</html>