
<%@ page import="org.hmp.Goal" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'goal.label', default: 'Goal')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-goal" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-goal" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'goal.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="threshold1" title="${message(code: 'goal.threshold1.label', default: 'Threshold1')}" />
					
						<g:sortableColumn property="threshold2" title="${message(code: 'goal.threshold2.label', default: 'Threshold2')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${goalInstanceList}" status="i" var="goalInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${goalInstance.id}">${fieldValue(bean: goalInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: goalInstance, field: "threshold1")}</td>
					
						<td>${fieldValue(bean: goalInstance, field: "threshold2")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${goalInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
