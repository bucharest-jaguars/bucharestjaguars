<%@ page import="org.hmp.Goal" %>



<div class="fieldcontain ${hasErrors(bean: goalInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="goal.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${goalInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: goalInstance, field: 'threshold1', 'error')} required">
	<label for="threshold1">
		<g:message code="goal.threshold1.label" default="Threshold1" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="threshold1" type="number" value="${goalInstance.threshold1}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: goalInstance, field: 'threshold2', 'error')} required">
	<label for="threshold2">
		<g:message code="goal.threshold2.label" default="Threshold2" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="threshold2" type="number" value="${goalInstance.threshold2}" required=""/>
</div>

