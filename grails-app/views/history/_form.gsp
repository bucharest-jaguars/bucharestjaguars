<%@ page import="org.hmp.History" %>



<div class="fieldcontain ${hasErrors(bean: historyInstance, field: 'user', 'error')} required">
    <label for="user">
        <g:message code="history.user.label" default="User"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select id="user" name="user.id" from="${org.hmp.User.list()}" optionKey="id" required=""
              value="${historyInstance?.user?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: historyInstance, field: 'date', 'error')} required">
    <label for="date">
        <g:message code="history.date.label" default="Date"/>
        <span class="required-indicator">*</span>
    </label>
    <g:datePicker name="date" precision="day" value="${historyInstance?.date}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: historyInstance, field: 'goal', 'error')} required">
    <label for="goal">
        <g:message code="history.goal.label" default="Goal"/>
        <span class="required-indicator">*</span>
    </label>
    <!--g:select id="goal" name="goal.id" from="${org.hmp.Goal.list()}" optionKey="id" required="" value="${historyInstance?.goal?.id}" class="many-to-one"/-->
    <table><tbody>
    <g:each in="${org.hmp.Goal.list()}" status="i" var="goalInstance">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
            <g:hiddenField name="goal${i}" value="${goalInstance.id}" />
            <td>${fieldValue(bean: goalInstance, field: "name")}</td>
            <td><g:textField name="value${i}" value="${historyInstance?.value}"/></td>
        </tr>
    </g:each>
    </tbody></table>
</div>
