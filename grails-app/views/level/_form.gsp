<%@ page import="org.hmp.Level" %>



<div class="fieldcontain ${hasErrors(bean: levelInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="level.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${levelInstance?.name}"/>
</div>

