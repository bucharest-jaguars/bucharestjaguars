<%@ page import="org.hmp.Content" %>



<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'goal', 'error')} required">
	<label for="goal">
		<g:message code="content.goal.label" default="Goal" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="goal" name="goal.id" from="${org.hmp.Goal.list()}" optionKey="id" required="" value="${contentInstance?.goal?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'level', 'error')} required">
	<label for="level">
		<g:message code="content.level.label" default="Level" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="level" name="level.id" from="${org.hmp.Level.list()}" optionKey="id" required="" value="${contentInstance?.level?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'uri', 'error')} required">
	<label for="uri">
		<g:message code="content.uri.label" default="Uri" />
		<span class="required-indicator">*</span>
	</label>
	
</div>

<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'data', 'error')} ">
	<label for="data">
		<g:message code="content.data.label" default="Data" />
		
	</label>
	<g:textField name="data" value="${contentInstance?.data}"/>
</div>

