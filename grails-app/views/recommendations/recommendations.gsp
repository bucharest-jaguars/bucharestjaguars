<%--
  Created by IntelliJ IDEA.
  User: btesila
  Date: 11/15/2014
  Time: 9:43 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="bootstrap"/>
    <title>Welcome to Bootstrap</title>
</head>

<body>
        <!-- Page Heading -->
<div class="row">
    <div class="col-lg-12" style="height: 300px">
        <h1 class="page-header">
            Recommendations for:
        </h1>
        <div class="col-sm-4">
        </div>
        <div class="col-sm-8">
            <div id="owl-demo" class="owl-carousel owl-theme">

                <div class="item" ><a href="#" class="recomm" id="weight">Weight</a></div>
                <div class="item"><a href="#" class="recomm" id="exercise">Exercise</a></div>
                <div class="item"><a href="#" class="recomm" id="outdoor">Outdoor</a></div>
                <div class="item"><a href="#" class="recomm" id="stress">Stress</a></div>
            </div>
    </div>
    </div>
</div>

<div class="row" id="recomm-div">
</div>

</body>
</html>