<html>
<head>
	<meta name='layout' content='login'/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>

<body style = "background: ${resource(dir: 'images', file: 'loginPage.jpg')}">
<div id="loginModal" class="modal show" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h1 class="text-center">Login</h1>
			</div>

			%{--<g:if test='${flash.message}'>--}%
				%{--<div class='login_message'>${flash.message}</div>--}%
			%{--</g:if>--}%

			<div class="modal-body">
			<form action='${postUrl}' method='POST' id='loginForm' class='form col-md-12 center-block' autocomplete='off'>
				<div class="form-group">
					<label for='username'><g:message code="springSecurity.login.username.label"/>:</label>
					<input type='text' class='form-control input-lg' name='j_username' id='username' placeholder="Username"/>
				</div>

				<div class="form-group">
					<input type='password' class='form-control input-lg' name='j_password' id='password' placeholder="Password"/>
				</div>

				<div id="remember_me_holder" class="form-group">
					<input type='checkbox' class='chk' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
					<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
				</div>

				<div class="form-group">
					<input type='submit' id="submit" class="btn btn-primary btn-lg btn-block" value='${message(code: "springSecurity.login.button")}'/>
					<span class="pull-right"><a href="${createLink(uri:'/user/create')}">Sign Up</a></span><span><a href="#">Need help?</a></span>
				</div>
			</form>
			</div>

			<div class="modal-footer">
				<div class="col-md-12">
					<button class="btn" data-dismiss="modal" aria-hidden="true" >Cancel</span></button>
				</div>
			</div>


		</div>
		</div>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>
