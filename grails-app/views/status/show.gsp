
<%@ page import="org.hmp.Status" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'status.label', default: 'Status')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-status" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-status" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list status">
			
				<g:if test="${statusInstance?.user}">
				<li class="fieldcontain">
					<span id="user-label" class="property-label"><g:message code="status.user.label" default="User" /></span>
					
						<span class="property-value" aria-labelledby="user-label"><g:link controller="user" action="show" id="${statusInstance?.user?.id}">${statusInstance?.user?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${statusInstance?.level}">
				<li class="fieldcontain">
					<span id="level-label" class="property-label"><g:message code="status.level.label" default="Level" /></span>
					
						<span class="property-value" aria-labelledby="level-label"><g:link controller="level" action="show" id="${statusInstance?.level?.id}">${statusInstance?.level?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${statusInstance?.goal}">
				<li class="fieldcontain">
					<span id="goal-label" class="property-label"><g:message code="status.goal.label" default="Goal" /></span>
					
						<span class="property-value" aria-labelledby="goal-label"><g:link controller="goal" action="show" id="${statusInstance?.goal?.id}">${statusInstance?.goal?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${statusInstance?.now}">
				<li class="fieldcontain">
					<span id="now-label" class="property-label"><g:message code="status.now.label" default="Now" /></span>
					
						<span class="property-value" aria-labelledby="now-label"><g:fieldValue bean="${statusInstance}" field="now"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${statusInstance?.toAchieve}">
				<li class="fieldcontain">
					<span id="toAchieve-label" class="property-label"><g:message code="status.toAchieve.label" default="To Achieve" /></span>
					
						<span class="property-value" aria-labelledby="toAchieve-label"><g:fieldValue bean="${statusInstance}" field="toAchieve"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${statusInstance?.id}" />
					<g:link class="edit" action="edit" id="${statusInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
