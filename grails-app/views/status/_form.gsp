<%@ page import="org.hmp.Status" %>



<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'user', 'error')} required">
	<label for="user">
		<g:message code="status.user.label" default="User" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="user" name="user.id" from="${org.hmp.User.list()}" optionKey="id" required="" value="${statusInstance?.user?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'level', 'error')} required">
	<label for="level">
		<g:message code="status.level.label" default="Level" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="level" name="level.id" from="${org.hmp.Level.list()}" optionKey="id" required="" value="${statusInstance?.level?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'goal', 'error')} required">
	<label for="goal">
		<g:message code="status.goal.label" default="Goal" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="goal" name="goal.id" from="${org.hmp.Goal.list()}" optionKey="id" required="" value="${statusInstance?.goal?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'now', 'error')} required">
	<label for="now">
		<g:message code="status.now.label" default="Now" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="now" type="number" value="${statusInstance.now}" required=""/>
</div>

<div class="fieldcontain ${hasErrors(bean: statusInstance, field: 'toAchieve', 'error')} required">
	<label for="toAchieve">
		<g:message code="status.toAchieve.label" default="To Achieve" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="toAchieve" type="number" value="${statusInstance.toAchieve}" required=""/>
</div>

