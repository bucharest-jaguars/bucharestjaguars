
<%@ page import="org.hmp.Status" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="bootstrap">
		<g:set var="entityName" value="${message(code: 'status.label', default: 'Status')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-status" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-status" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<th><g:message code="status.user.label" default="User" /></th>
					
						<th><g:message code="status.level.label" default="Level" /></th>
					
						<th><g:message code="status.goal.label" default="Goal" /></th>
					
						<g:sortableColumn property="now" title="${message(code: 'status.now.label', default: 'Now')}" />
					
						<g:sortableColumn property="toAchieve" title="${message(code: 'status.toAchieve.label', default: 'To Achieve')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${statusInstanceList}" status="i" var="statusInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${statusInstance.id}">${fieldValue(bean: statusInstance, field: "user")}</g:link></td>
					
						<td>${fieldValue(bean: statusInstance, field: "level")}</td>
					
						<td>${fieldValue(bean: statusInstance, field: "goal")}</td>
					
						<td>${fieldValue(bean: statusInstance, field: "now")}</td>
					
						<td>${fieldValue(bean: statusInstance, field: "toAchieve")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${statusInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
